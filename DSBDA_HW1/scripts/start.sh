#!/usr/bin/env bash
hadoop jar /usr/lib/hadoop-mapreduce/hadoop-streaming.jar \
-D mapreduce.job.name="[Streaming mode][Searching of the longest word][task1]" \
-files src/mapper.py,src/reducer.py \
-inputformat org.apache.hadoop.mapred.TextInputFormat \
-outputformat org.apache.hadoop.mapred.SequenceFileOutputFormat \
-input files/task1/input \
-output files/task1/output \
-mapper mapper.py \
-reducer reducer.py


#-file ./src/mapper.py -mapper mapper.py \
#-file ./src/reducer.py -reducer reducer.py \
#-files file:///home/cloudera/Downloads/task1/src/mapper.py,file:///home/cloudera/Downloads/task1/src/reducer.py \
#-files hdfs://localhost/user/cloudera/files/task1/src/mapper.py,hdfs://localhost/user/cloudera/files/task1/src/reducer.py \
