#!/usr/bin/env python
"""Module realise class Mapper"""
import sys
import re


class Mapper(object):
    def __init__(self, input=sys.stdin, output=sys.stdout):
        """Keep input and output streams as parameters"""
        self.data = {}
        self.input = input
        self.output = output
        self.maxValue = 0

    def write(self):
        """Write in output stream"""
        for (key, value) in self.data.items():
            self.output.write(key + '\t' + str(value) + '\n')

    def map(self):
        """Realise map stage of map-reduce task"""
        for token in self.input:
            for word in re.finditer(r'\b([a-zA-Z]+)\b', token):
                if self.maxValue < len(word.group()):
                    self.maxValue = len(word.group())
                    self.data = {word.group(): self.maxValue}
                elif word.group() not in self.data and self.maxValue == len(word.group()):
                    self.data[word.group()] = self.maxValue
        self.write()


if __name__ == '__main__':
    mapper = Mapper()
    mapper.map()
