#!/usr/bin/env python
"""Module realise class Reducer"""
import sys


class Reducer(object):
    def __init__(self, input=sys.stdin, output=sys.stdout):
        """Keep input and output streams as parameters"""
        self.data = {}
        self.input = input
        self.output = output
        self.maxValue = 0

    def write(self):
        """Write in output stream"""
        for (key, value) in self.data.items():
            self.output.write(key + '\t' + str(value) + '\n')

    def reduce(self):
        """Realise reduce stage of map-reduce task"""
        for token in self.input:
            (key, value) = token.strip().strip('\n').split('\t')
            if self.maxValue < int(value):
                self.maxValue = int(value)
                self.data = {key: self.maxValue}
            elif key not in self.data and self.maxValue == int(value):
                self.data[key] = self.maxValue
        self.write()


if __name__ == '__main__':
    reducer = Reducer()
    reducer.reduce()
