#!/usr/bin/env python
"""Module realise set of unit tests for classes Mapper and Reducer"""
import unittest
import io
import mapper as mp
import reducer as rd


class TestMapper(unittest.TestCase):
    def test_simple_word(self):
        word = unicode('SimpleWord')
        output = io.StringIO()
        mapper = mp.Mapper(input=io.StringIO(word), output=output)
        mapper.map()
        self.assertEqual(
            output.getvalue().strip(),
            word + '\t' + str(len(word))
        )

    def test_equal_words(self):
        words = map(unicode, ['One', 'Two', 'Six', 'Ten'])
        output = io.StringIO()
        mapper = mp.Mapper(input=io.StringIO('\t'.join(words)), output=output)
        mapper.map()
        self.assertEqual(
            sorted(output.getvalue().strip().split('\n')),
            sorted(map(lambda x: x + '\t' + str(len(x)), words))
        )

    def test_not_equal_words(self):
        words = map(unicode, ['One', 'Two', 'Three', 'Four', 'Five'])
        maxLength = max(map(len, words))
        maxWord = filter(lambda x: len(x) == maxLength, words)[0]
        output = io.StringIO()
        mapper = mp.Mapper(input=io.StringIO('\t'.join(words)), output=output)
        mapper.map()
        self.assertEqual(
            output.getvalue().strip(),
            maxWord + '\t' + str(maxLength)
        )

    def test_separated_words(self):
        words = [
            'Start', 'One', 'Two', 'Three', 'Four', 'Five', 'Six',
            'Seven', 'Eight', 'Nine', 'Ten', 'Eleven', 'End'
        ]
        maxLength = max(map(len, words))
        maxWord = filter(lambda x: len(x) == maxLength, words)[0]
        separators = [
            '-', '=', '.', ',', '$', '(', ')',
            '{', '}', '[', ']', '/', "'"
        ]
        string = ''.join(map(lambda (x, y): unicode(''.join((x, y))), zip(words, separators)))
        output = io.StringIO()
        mapper = mp.Mapper(input=io.StringIO(string), output=output)
        mapper.map()
        self.assertEqual(
            output.getvalue().strip(),
            maxWord + '\t' + str(maxLength)
        )


class TestReducer(unittest.TestCase):
    def test_simple_string(self):
        string = unicode('SimpleWord\t10')
        output = io.StringIO()
        reducer = rd.Reducer(input=io.StringIO(string), output=output)
        reducer.reduce()
        self.assertEqual(
            output.getvalue().strip(),
            string
        )

    def test_equal_strings(self):
        words = map(unicode, ['One\t3', 'Two\t3', 'Six\t3', 'Ten\t3'])
        output = io.StringIO()
        reducer = rd.Reducer(input=io.StringIO('\n'.join(words)), output=output)
        reducer.reduce()
        self.assertEqual(
            sorted(output.getvalue().strip().split('\n')),
            sorted(words)
        )

    def test_not_equal_strings(self):
        words = map(unicode, ['One\t3', 'Two\t3', 'Three\t5', 'Four\t4', 'Five\t4'])
        maxLength = max(map(len, words))
        maxWord = filter(lambda x: len(x) == maxLength, words)[0]
        output = io.StringIO()
        reducer = rd.Reducer(input=io.StringIO('\n'.join(words)), output=output)
        reducer.reduce()
        self.assertEqual(
            output.getvalue().strip(),
            maxWord
        )


if __name__ == '__main__':
    unittest.main()
