#!/usr/bin/env bash
sudo yum install python;
sudo yum install mysql;
sudo yum install mysql-devel;
sudo yum install epel-release;
sudo yum install python34;
sudo yum install python34-devel;
sudo yum install gcc;
#sudo wget https://bootstrap.pypa.io/get-pip.py;
sudo python3 get-pip.py;
sudo pip3 install sqlalchemy;
sudo pip3 install configparser;
sudo pip3 install mysqlclient;
sudo pip3 install pandas;
