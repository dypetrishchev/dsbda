"""Module realises class RDD"""
from pyspark import SparkContext
from pyspark.sql import SQLContext, Row
import pyspark.sql.functions as func
from pyspark.sql.types import DecimalType
import sys
import os


class RDD(object):
    def __init__(self, input=sys.stdin):
        """Keep input object"""
        self.input = input
        self.sc = SparkContext(appName='Spark SQL Task')
        self.spark = SQLContext(self.sc)
        self.lines = None
        self.rows = None
        self.schema_name = None
        self.df = None

    def read(self):
        """Read text file with data"""
        self.lines = self.sc.textFile(self.input)

    def parse(self, col_idx={}, sep=','):
        """Parse text lines to tokens"""
        tokens = self.lines.map(lambda x: x.strip().split(sep))
        self.rows = tokens.map(
            lambda x: Row(
                **{key: x[value] for (key, value) in col_idx.items()}
            )
        )

    def createDataFrame(self, schema_name='schema'):
        """Create DataFrame and register schema"""
        self.schema_name = schema_name
        self.df = self.spark.createDataFrame(self.rows)
        self.df.registerTempTable(schema_name)

    def read_create(self, col_idx={}, sep=','):
        """Read text file, parse lines, create DataFrame, register schema"""
        self.read()
        self.parse(col_idx=col_idx, sep=sep)
        self.createDataFrame()


if __name__ == '__main__':
    path_from = '/user/cloudera/person_m_agg/'
    fname = 'part-m-[0-9]*'
    full_path = os.path.join(path_from, fname)
    rdd = RDD(input=full_path)
    col_idx = {
        'pass_num': 1,
        'birth_date': 2,
        'salary_amt': 3,
        'trip_cnt': 4,
        'value_day': 5
    }
    rdd.read_create(col_idx=col_idx, sep=',')
    df = rdd.df.select(
        func.floor(func.floor(
            func.months_between(rdd.df['value_day'], rdd.df['birth_date']) / 12
        ) / 10).alias('age_category'),
        rdd.df['salary_amt'],
        rdd.df['trip_cnt']
    )
    df = df.groupBy('age_category').agg(
        func.avg('salary_amt').cast(DecimalType(10, 2)).alias('avg_salary_amt'),
        func.avg('trip_cnt').cast(DecimalType(10, 2)).alias('avg_trip_cnt')
    ).orderBy(func.asc('age_category'))
    df = df.select(
        func.concat_ws('-', df['age_category']*10, df['age_category']*10 + 9).alias('age_category'),
        df['avg_salary_amt'],
        df['avg_trip_cnt']
    ).orderBy(func.asc('age_category'))
    path_to = os.path.join(path_from, 'output')
    df \
        .coalesce(1) \
        .write \
        .save(path_to, format='json')
    df.show()
    rdd.sc.stop()

