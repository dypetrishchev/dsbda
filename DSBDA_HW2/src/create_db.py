#!/usr/bin/env python3
"""Module realise class Person"""
from sqlalchemy import BigInteger, Column, Date, Numeric, Integer
from sqlalchemy.ext.declarative import declarative_base
from engine import engine


Base = declarative_base()


class Person(Base):
    """Class realises Object-Relational Mapping"""
    __tablename__ = 'person_m_agg'

    pk = Column(BigInteger, primary_key=True)
    pass_num = Column(BigInteger, nullable=False)
    birth_date = Column(Date, nullable=False)
    salary_amt = Column(Numeric(10, 2), nullable=True)
    trip_cnt = Column(Integer, nullable=True)
    value_day = Column(Date, nullable=False)

    def __repr__(self):
        return ('(pk=%s, pass_num=%s, birth_date=%s, ' +
                'salary_amt=%s, trip_cnt=%s, value_day=%s)') % (
            self.pk, self.pass_num, self.birth_date,
            self.salary_amt, self.trip_cnt, self.value_day
        )


if __name__ == '__main__':
    Base.metadata.create_all(engine)
