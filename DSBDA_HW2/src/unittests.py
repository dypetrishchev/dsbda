"""Module realises set of unit tests for class RDD"""
import unittest
from spark import RDD
import os
import pandas as pd
from pandas.util.testing import assert_frame_equal


class TestDataFrame(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        """Define common variables"""
        super(TestDataFrame, self).__init__(*args, **kwargs)
        self.string = 'This is a test string 1\nThis is a test string 2'
        self.path = os.path.dirname(os.path.abspath(__file__))
        self.fname = 'test.txt'
        self.full_path = os.path.join(self.path, self.fname)

    def test_read(self):
        """Test reading from text file"""
        try:
            with open(self.full_path, mode='wt', encoding='utf-8') as f:
                f.write(self.string)
            rdd = RDD(input=self.full_path)
            rdd.read()
            self.assertEqual(
                rdd.lines.collect(),
                self.string.split('\n')
            )
        finally:
            os.remove(self.full_path)

    def test_parse(self):
        """Test parsing of text lines"""
        try:
            with open(self.full_path, mode='wt', encoding='utf-8') as f:
                f.write(self.string)
            rdd = RDD(input=self.full_path)
            rdd.read()
            idxs = list(range(len(self.string.split('\n')[0].split(' '))))
            cols = ['col' + str(i) for i in idxs]
            rdd.parse(sep=' ', col_idx=dict(zip(cols, idxs)))
            self.assertEqual(
                sorted(list(rdd.rows.collect()[0].asDict().values())),
                sorted(self.string.split('\n')[0].split(' '))
            )
        finally:
            os.remove(self.full_path)

    def test_createDataFrame(self):
        """Test creating of DataFrame and registering of schema"""
        try:
            with open(self.full_path, mode='wt', encoding='utf-8') as f:
                f.write(self.string)
            rdd = RDD(input=self.full_path)
            rdd.read()
            data = list(map(lambda x: x.split(' '), self.string.split('\n')))
            idxs = list(range(len(data[0])))
            cols = ['col' + str(i) for i in idxs]
            rdd.parse(sep=' ', col_idx=dict(zip(cols, idxs)))
            rdd.createDataFrame()
            assert_frame_equal(
                rdd.df.toPandas(),
                pd.DataFrame(data=data, columns=cols)
            )
        finally:
            os.remove(self.full_path)


if __name__ == '__main__':
    unittest.main()

