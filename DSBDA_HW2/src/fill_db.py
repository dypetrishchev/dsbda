#!/usr/bin/env python3
"""Module generates data and fills database"""
from calendar import monthrange
from create_db import Person
from datetime import date, timedelta
from engine import Session
from random import gauss, randint


def random_date(start_year, end_year):
    """Generate random data between start_year and end_year"""
    days_between = date(end_year, 12, 31) - date(start_year, 1, 1)
    delta = round(abs(gauss(mu=0, sigma=days_between.days/3)))
    if delta > days_between.days:
        delta = randint(2*days_between.days - delta, days_between.days)
    return date(end_year, 12, 31) - timedelta(delta)


if __name__ == '__main__':
    N_pass, N_flush = 10**4, 10**3
    min_year, max_year, current_year = 1927, 1999, 2017
    avg_salary_amt, avg_trip_cnt = 60*10**3, 2
    session = Session()
    for idx, pass_num in enumerate(x for x in range(10**6, 10**6 + N_pass)):
        birth_date = random_date(min_year, max_year)
        for month in range(1, 13):
            salary_amt = abs(gauss(mu=avg_salary_amt, sigma=avg_salary_amt/3))
            trip_cnt = round(salary_amt / avg_salary_amt * avg_trip_cnt)
            value_day = date(current_year, month, monthrange(current_year, month)[-1])
            person = Person(
                pass_num=pass_num,
                birth_date=birth_date,
                salary_amt=salary_amt,
                trip_cnt=trip_cnt,
                value_day=value_day
            )
            session.add(person)
        if idx % N_flush == 0:
            session.commit()
    session.commit()
