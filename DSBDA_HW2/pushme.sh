#!/usr/bin/env bash
source scripts/install.sh;
mysql -uroot -pcloudera < scripts/create_user.sql;
python3 src/create_db.py;
python3 src/fill_db.py;
source src/import_db.sh;
hadoop fs -ls /user/cloudera/person_m_agg/output;
